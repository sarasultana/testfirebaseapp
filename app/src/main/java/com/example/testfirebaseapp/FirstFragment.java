package com.example.testfirebaseapp;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

public class FirstFragment extends Fragment implements OnClickListener{

    View v;
    EditText email;
    EditText pwd;
    Button reg;
    TextView txt;
    ProgressDialog progress;
    FirebaseAuth fbAuth;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_first, container, false);
        fbAuth = FirebaseAuth.getInstance();
        email = (EditText) v.findViewById(R.id.username);
        pwd = (EditText) v.findViewById(R.id.password);
        reg = (Button) v.findViewById(R.id.register_btn);
        txt = (TextView) v.findViewById(R.id.txt1);
        progress = new ProgressDialog(getActivity());

        reg.setOnClickListener(this);
        txt.setOnClickListener(this);
        return v;
    }

    private void registerUser()
    {

        String emailid = email.getText().toString().trim();
        String pass = pwd.getText().toString().trim();
        if(TextUtils.isEmpty(emailid))
        {
            Toast.makeText(getActivity(), "Please enter email id", Toast.LENGTH_SHORT).show();
            return;
        }
        if(TextUtils.isEmpty(pass))
        {
            Toast.makeText(getActivity(), "Please enter password", Toast.LENGTH_SHORT).show();
            return;
        }

        progress.setMessage("Please wait ...");
        progress.show();
        fbAuth.createUserWithEmailAndPassword(emailid,pass).addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task)
            {
                if(task.isSuccessful())
                {
                    Toast.makeText(getActivity(), "Registration complete", Toast.LENGTH_SHORT).show();
                    progress.dismiss();
                    NavHostFragment.findNavController(FirstFragment.this).navigate(R.id.action_FirstFragment_to_SecondFragment);
                }
                else
                    Toast.makeText(getActivity(),"Registration failed, please try again", Toast.LENGTH_SHORT).show();
            }
        });

    }

   @Override
    public void onClick(View v) {
        if (v == reg)
            registerUser();
        if (v == txt)
            NavHostFragment.findNavController(FirstFragment.this).navigate(R.id.action_FirstFragment_to_loginFragment);

   }
}
