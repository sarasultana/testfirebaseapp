package com.example.testfirebaseapp;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class loginFragment extends Fragment implements View.OnClickListener {
    View v;
    EditText email;
    EditText pwd;
    Button login;
    ProgressDialog progress;
    FirebaseAuth fbAuth;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_login, container, false);
        fbAuth = FirebaseAuth.getInstance();
        email = (EditText) v.findViewById(R.id.login_username);
        pwd = (EditText) v.findViewById(R.id.login_password);
        login = (Button) v.findViewById(R.id.login_btn);
        progress = new ProgressDialog(getActivity());

        login.setOnClickListener(this);
        return v;

    }

    @Override
    public void onClick(View v)
    {
        String idstr = email.getText().toString().trim();
        String pwdstr = pwd.getText().toString().trim();
        if (TextUtils.isEmpty(idstr)) {
            Toast.makeText(getContext(), "Please enter email", Toast.LENGTH_LONG).show();
            return;
        }

        if (TextUtils.isEmpty(pwdstr)) {
            Toast.makeText(getContext(), "Please enter password", Toast.LENGTH_LONG).show();
            return;
        }
        progress.setMessage("Please wait ...");
        progress.show();
        fbAuth.signInWithEmailAndPassword(idstr, pwdstr).addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                progress.dismiss();
                if (task.isSuccessful()) {
                    Toast.makeText(getContext(), "Login successful !", Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(getActivity(),getDetails.class);
                    startActivity(intent);
                }
            }
        });
    }
}


