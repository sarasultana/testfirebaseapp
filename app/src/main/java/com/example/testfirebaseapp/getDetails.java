package com.example.testfirebaseapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

public class getDetails extends AppCompatActivity

{
    EditText name, loc, qual;
    Button save;
    String name1,location,qualification;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_details);

        name = (EditText)findViewById(R.id.editText_name);
        loc = (EditText)findViewById(R.id.editText_loc);
        qual = (EditText)findViewById(R.id.editText_qual);
        save = (Button)findViewById(R.id.save_button);

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ProgressDialog loading = ProgressDialog.show(getDetails.this,
                        "Please wait...", "Saving data ...", true);
                name1 = name.getText().toString().trim();
                location = loc.getText().toString().trim();
                qualification = qual.getText().toString().trim();

                if (!isExternalStorageAvailable() || isExternalStorageReadOnly()) {
                    Toast.makeText(getApplicationContext(), "No External Storage", Toast.LENGTH_SHORT).show();
                    Log.w("FileUtils", "Storage not available or read only");

                }

                FileOutputStream fos = null;
                ActivityCompat.requestPermissions(getDetails.this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        1);
                ActivityCompat.requestPermissions(getDetails.this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        1);


                Workbook workbook;
                Sheet sheet;

                try {
                    File file = new File(getApplicationContext().getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS), "testinganothertest.xls");
                    if (file.exists())
                    {
                        workbook=WorkbookFactory.create(file);
                    sheet = workbook.getSheetAt(0);
                    int lastRow=sheet.getLastRowNum();
                    lastRow++;
                    Row row = sheet.createRow(lastRow);
                    row.createCell(0).setCellValue(name1);
                    row.createCell(1).setCellValue(location);
                    row.createCell(2).setCellValue(qualification);
                        fos = new FileOutputStream(file);
                        workbook.write(fos);
                        System.out.print("control is here");
                        fos.flush();
                        fos.close();
                    }
                    else
                        {
                        workbook=new HSSFWorkbook();
                        sheet = workbook.createSheet("testsheet");
                        Row row = sheet.createRow(0);
                        row.createCell(0).setCellValue(name1);
                        row.createCell(1).setCellValue(location);
                        row.createCell(2).setCellValue(qualification);
                        //rownum++;
                            fos = new FileOutputStream(file);
                            workbook.write(fos);
                            System.out.print("control is here");
                            fos.flush();
                            fos.close();
                    }

                    }
                    catch (IOException e)
                    {
                        e.printStackTrace();
                    }

                        loading.dismiss();
                        Toast.makeText(getDetails.this, "Excel Sheet Generated at: "+ getApplicationContext().getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS), Toast.LENGTH_LONG).show();
                        LayoutInflater inflater = (LayoutInflater) getApplicationContext().getSystemService(LAYOUT_INFLATER_SERVICE);
                        View customView = inflater.inflate(R.layout.popup_layout, null);
                        PopupWindow popW = new PopupWindow(customView, 1000, 1000);
                        Button yesbtn = (Button) customView.findViewById(R.id.yes_btn);
                        yesbtn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v)
                            {
                                name.setText("");
                                loc.setText("");
                                qual.setText("");
                                popW.dismiss();
                            }
                        });
                        Button logoutbtn = (Button) customView.findViewById(R.id.no_btn);
                        logoutbtn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v)
                            {
                                FirebaseAuth.getInstance().signOut();
                                Intent intent = new Intent(getDetails.this, MainActivity.class);
                                startActivity(intent);
                            }
                        });

                        popW.showAtLocation(getCurrentFocus(), Gravity.CENTER, 0, 0);

            }});
    }
 public static boolean isExternalStorageReadOnly() {
        String extStorageState = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(extStorageState)) {
            return true;
        }
        return false;
    }

    public static boolean isExternalStorageAvailable() {
        String extStorageState = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(extStorageState)) {
            return true;
        }
        return false;
    }

}
